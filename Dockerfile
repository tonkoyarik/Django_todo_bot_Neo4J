FROM python:3.6
# Ensure that Python outputs everything that's printed inside
# the application rather than buffering it.
ENV PYTHONUNBUFFERED 1
RUN mkdir /code/
WORKDIR /code/
ADD . /code/
# Creation of the workdir
# Add requirements.txt file to container
# Install requirements
ADD requirements.txt /requirements.txt
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
# Add the current directory(the web folder) to Docker container
