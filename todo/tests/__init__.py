from todo.models import User, Todo


def setup():
    user = User(messenger_id="874323873872623", first_name="Yarik")
    user.save()
    todo = Todo(
        title="test title",
        image_url="http://www.halo.com",
        description="subtitle1",
    )
    todo.save()
    connection_1 = user.todo.connect(todo)
    user_2 = User(messenger_id="28736882827648", first_name="John")
    user_2.save()
    todo_2 = Todo(
        title="test title_2",
        image_url="http://www.halo.com",
        description="subtitle1",
    )
    todo_2.save()
    connection_2 = user_2.todo.connect(todo_2)
    user_3 = User(messenger_id="984976237834", first_name="Nick")
    user_3.save()
    todo_3 = Todo(
        title="test title_3",
        image_url="http://www.test2.com",
        description="subtitle2",
    )
    todo_3.save()
    conection_3 = user_2.todo.connect(todo_3)
