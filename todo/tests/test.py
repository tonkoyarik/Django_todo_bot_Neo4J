
from django.test import TestCase
from neomodel import db

from todo.db.todo import get_single_todo, get_todo_with_user
from todo.models import User
from todo.tests import setup


class TodoTestCase(TestCase):
    def setUp(self):
        setup()

    def tearDown(self):
        query = """MATCH (n:Todo), (u:User) detach delete n,u"""
        remove_data = db.cypher_query(query)

    def test_todo_get_several_todos(self):
        response = self.client.get("/todo/", {"messenger user id": "28736882827648"})
        res = response.data["messages"][0]["attachment"]["payload"]["elements"]
        self.assertEqual(len(res), 2)
        self.assertEqual(response.status_code, 200)

    def test_todo_get_single_todo(self):
        response = self.client.get("/todo/", {"messenger user id": "874323873872623"})
        expected_button = "This is your first TODO"
        self.assertContains(response, expected_button)

    def test_todo_post(self):
        record1 = {
            "title": "title1",
            "image_url": "hello",
            "description": "subtitle1",
            "date_time": "12/12/2017 23:50",
            "messenger user id": "874323873872623",
        }
        response = self.client.post("/todo/", record1)
        self.assertEqual(response.status_code, 201)

    def test_crete_user(self):
        reporter = {"first name": "Yarik", "messenger user id": "28736388287642"}
        response = self.client.post("/welcome/", reporter)
        res_data = response.data
        user = User.nodes.get(messenger_id="28736388287642")
        messenger_id = res_data.get("messenger_id")
        self.assertEqual(int(messenger_id), user.messenger_id)

    def test_todo_delete(self):
        user = User.nodes.get(messenger_id=874323873872623)
        task = get_todo_with_user(user)
        task_id = task[0].get("id")
        response = self.client.delete(f"/delete/{task_id}/")
        check_todo = get_single_todo(task_id)
        self.assertTrue(task)
        self.assertIsNone(check_todo)
