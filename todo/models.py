from datetime import datetime

import pytz
from neomodel import (
    StringProperty,
    StructuredNode,
    RelationshipTo,
    RelationshipFrom,
    IntegerProperty,
    DateTimeProperty,
    UniqueIdProperty,
    StructuredRel,
)


class ScoredRel(StructuredRel):
    datetime = DateTimeProperty(default=lambda: datetime.now(pytz.utc))


class User(StructuredNode):
    uid = UniqueIdProperty()
    first_name = StringProperty()
    messenger_id = IntegerProperty(unique_index=True)
    todo = RelationshipTo("Todo", "HAS_TODO")


class Todo(StructuredNode):
    uid = UniqueIdProperty()
    title = StringProperty()
    image_url = StringProperty()
    description = StringProperty()
    date_time = DateTimeProperty(default_now=True)
    user = RelationshipFrom("User", "HAS_TODO", model=ScoredRel)
