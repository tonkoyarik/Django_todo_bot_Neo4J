from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework.views import APIView

from todo.db.todo import remove_todo, add_todo, get_todo_with_user
from todo.db.user import add_user, get_user, delete_user
from todo.response_helper import todo_list_response, error_todo, added_todo
from todo.serializer import TodoSerializer
from rest_framework import status


class Welcome(APIView):
    def get(self, request):
        response = {"hello": "Welcome to the TODO bot"}
        return Response(response)


class Delete(APIView):
    def delete(self, request, task_id):
        removed_todo = remove_todo(task_id)
        if not removed_todo:
            msg = "Todo does not exist"
            return Response({"message": msg}, status=status.HTTP_404_NOT_FOUND)
        response = {
            "messages": [{"text": "Selected task has been successfully removed"}]
        }
        return Response(response, status=status.HTTP_200_OK)


class UserView(APIView):
    def post(self, request):
        payload = request.data
        check_user = get_user(payload.get("messenger user id"))
        if check_user:
            return Response("User already exists", status=status.HTTP_400_BAD_REQUEST)
        added_user = add_user(payload)
        return Response(added_user, status=status.HTTP_201_CREATED)

    def delete(self, request, messenger_id):
        removed_user = delete_user(messenger_id)
        if not removed_user:
            return Response("User does not exist", status=status.HTTP_200_OK)
        return Response(removed_user, status=status.HTTP_200_OK)


class TodoView(APIView):
    def get(self, request):
        data = request.GET
        messenger_id = data["messenger user id"]
        user = get_user(messenger_id)
        if not user:
            msg = "User does not exist"
            return Response({"message": msg}, status=status.HTTP_404_NOT_FOUND)
        user_todo = get_todo_with_user(user)
        todos = todo_list_response(user_todo)
        return Response(todos, status=status.HTTP_200_OK)

    def post(self, request):
        data = request.data
        request_user = data.get("messenger user id")
        user = get_user(request_user)
        if not user:
            msg = "User does not exist"
            return Response({"message": msg}, status=status.HTTP_404_NOT_FOUND)
        serializer = TodoSerializer(data=data)

        try:
            serializer.is_valid(raise_exception=True)
        except ValidationError as r:
            print(r)
            return Response(error_todo, status=status.HTTP_400_BAD_REQUEST)

        validated_data = serializer.validated_data
        todo = add_todo(validated_data, user)
        return Response(added_todo, status=status.HTTP_201_CREATED)
