from typing import List, Dict

added_todo = {
    "messages": [
        {"text": "You have successfully added a new task, let's check the list now!"}
    ]
}


error_todo = {
    "messages": [
        {
            "text": "You need you to enter Title, Description and DataTime in format (dd/mm/yy 23:50)"
        }
    ]
}


def prepare_response_data(todo_list: List) -> List:
    """

    Args:
        todo_list: List of todos

    Returns:
        List of todos adapted for response

    """
    for todo in todo_list:
        todo["image_url"] = "https://paulund.co.uk/app/uploads/2016/10/todo-list.png"
        date_time = todo["date_time"]
        description = todo["description"]
        todo["description"] = f"{description} \n Date: {date_time}"
        task_id = todo.pop("uid")
        todo["buttons"] = [
            {
                "url": f"https://0a590de0.ngrok.io/delete/{task_id}/",
                "type": "json_plugin_url",
                "title": "{}".format("Remove"),
            }
        ]
        del todo["date_time"]
    return todo_list


def todo_list_response(todo_list: List) -> Dict:
    """

    Args:
        todo_list: List of todos

    Returns:
        Json (dict) response in required format
    """
    if len(todo_list) == 1:
        todo = todo_list[0]
        title = todo.get("title")
        description = todo.get("description")
        date_time = todo.get("date_time")
        response = {
            "messages": [
                {
                    "text": f"This is your first TODO Subject:{title}\n:{description}\n Date:{date_time}"
                }
            ]
        }
        return response
    # TODO: this part has to be generated dinamically. Find the way not to hardcode this part
    elif len(todo_list) > 4:
        todo_elements = prepare_response_data(todo_list)
        response = {
            "messages": [
                {
                    "attachment": {
                        "type": "template",
                        "payload": {
                            "template_type": "list",
                            "top_element_style": "large",
                            "elements": todo_elements[:3],
                        },
                    }
                },
                {
                    "attachment": {
                        "type": "template",
                        "payload": {
                            "template_type": "list",
                            "top_element_style": "large",
                            "elements": todo_elements[3:6],
                        },
                    }
                },
                {
                    "attachment": {
                        "type": "template",
                        "payload": {
                            "template_type": "list",
                            "top_element_style": "large",
                            "elements": todo_elements[6:9],
                        },
                    }
                },
                {
                    "attachment": {
                        "type": "template",
                        "payload": {
                            "template_type": "list",
                            "top_element_style": "large",
                            "elements": todo_elements[9:11],
                        },
                    }
                },
            ]
        }
        return response
    todo_elements = prepare_response_data(todo_list)
    response = {
        "messages": [
            {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "list",
                        "top_element_style": "large",
                        "elements": todo_elements,
                    },
                }
            }
        ]
    }
    return response
