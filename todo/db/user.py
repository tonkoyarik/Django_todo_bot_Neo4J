from typing import Dict, Union

from neomodel import DoesNotExist

from todo.models import User
from todo.serializer import UserSerializer


def get_user(messeger_id: int) -> Union[User, None]:
    """

    Args:
        messeger_id: messenger id from request

    Returns:
        User object
    """
    try:
        user = User.nodes.get(messenger_id=messeger_id)
    except DoesNotExist:
        return None
    return user


def add_user(payload: Dict) -> Dict:
    """
    Add user to the Db

    Args:
        payload: User data from payload

    Returns:
        Added user
    """
    data = dict()
    data["first_name"] = payload["first name"]
    data["messenger_id"] = payload["messenger user id"]
    user = User(**data)
    user.save()
    serialized = UserSerializer(user)
    return serialized.data


def delete_user(messenger_id: int):
    """

    Args:
        user: fb messenger id

    Returns:
        removed user
    """
    try:
        user = User.nodes.get(messenger_id=messenger_id)
    except DoesNotExist:
        return None
    delete_user = user.delete()
    serialized_dict = UserSerializer(user)
    return serialized_dict.data
