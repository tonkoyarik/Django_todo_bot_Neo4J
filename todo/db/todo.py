from typing import Dict, List, Union

from neomodel import DoesNotExist

from todo.models import Todo, User
from todo.serializer import TodoSerializer


def add_todo(payload: Dict, user: User) -> Dict:
    """
    Add new task
    Args:
        payload: todo data
        user: user messenger id

    Returns:
        db object
    """

    new_rec = Todo(
        title=payload["title"],
        image_url=payload["image_url"],
        description=payload["description"],
        date_time=payload["date_time"],
    ).save()
    user.todo.connect(new_rec)
    serialized = TodoSerializer(new_rec)
    return serialized.data


def remove_todo(task: int) -> Union[Dict, None]:
    """
    Remove task from db
    Args:
        task: task id

    Returns:
        db object
    """
    try:
        todo = Todo.nodes.get(uid=task)
    except DoesNotExist:
        return None
    removed_todo = todo.delete()
    return todo


def get_todo_with_user(user: User) -> List:
    """
    Args:
        user: User object

    Returns:
        All user todos
    """
    todos = user.todo.all()
    serialized = TodoSerializer(todos, many=True)
    return serialized.data


def get_single_todo(todo: int) -> Union[Dict, None]:
    """
    Args:
        todo: id

    Returns:
        Todo object
    """
    try:
        todo = Todo.nodes.get(uid=todo)
    except Todo.DoesNotExist:
        return None
    serialized = TodoSerializer(todo)
    return serialized.data
