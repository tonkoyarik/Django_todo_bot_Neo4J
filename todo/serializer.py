from datetime import datetime
from rest_framework import serializers
from todo.models import Todo, User


class TodoSerializer(serializers.Serializer):
    uid = serializers.CharField(required=False)
    title = serializers.CharField(max_length=100)  # Like a VARCHAR field
    image_url = serializers.CharField(allow_null=True)  # Like a TEXT field
    description = serializers.CharField(allow_null=True)
    date_time = serializers.CharField(allow_null=True)

    def validate(self, attrs):
        if attrs.get("date_time"):
            l = [
                "%d/%m/%y %H:%M",
                "%m/%d/%y %H:%M",
                "%d/%m/%y %I:%M",
                "%d/%m/%Y %I:%M",
                "%m/%d/%y %I:%M",
                "%d/%m/%y %I:%M%p",
                "%d/%m/%Y %H:%M",
                "%d/%m/%Y",
                "%m/%d/%Y",
                "%d/%m/%y" "%I:%M",
                "%H:%M",
                "%d-%m-%y %H:%M",
                "%m-%d-%y %H:%M",
                "%d-%m-%y %I:%M",
                "%d-%m-%Y %I:%M",
                "%m-%d-%y %I:%M",
                "%d-%m-%y %I:%M%p",
                "%d-%m-%Y %H:%M",
                "%d-%m-%Y",
                "%d-%m-%y" "%I:%M",
                "%I:%M",
                "%d.%m.%y %H:%M",
                "%m.%d.%y %H:%M",
                "%d.%m.%y %I:%M",
                "%d.%m.%Y %I:%M",
                "%m.%d.%y %I:%M",
                "%d.%m.%y %I:%M%p",
                "%d.%m.%Y %H:%M",
                "%d.%m.%Y",
                "%d.%m.%y" "%I:%M",
                "%d.%m.%y",
                "%m.%d.%Y",
                "%m.%d.%y",
            ]
            for pattern in l:
                try:
                    dt = datetime.strptime(attrs["date_time"], pattern)
                    attrs["date_time"] = dt
                    return attrs
                    break
                except ValueError:
                    continue
            else:
                raise serializers.ValidationError(
                    "Please enter the data in correct format"
                )


class UserSerializer(serializers.Serializer):
    first_name = serializers.CharField(max_length=30)
    messenger_id = serializers.CharField()
