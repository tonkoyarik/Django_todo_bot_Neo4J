from django.contrib import admin
from django.urls import path

from todo import view

urlpatterns = [
    path("", view.Welcome.as_view(), name="home"),
    path("admin/", admin.site.urls),
    path("todo/", view.TodoView.as_view()),
    path("welcome/", view.UserView.as_view()),
    path("delete/<task_id>/", view.Delete.as_view(), name="remove task"),
    path("remove_user/<int:messenger_id>/", view.UserView.as_view()),
]
